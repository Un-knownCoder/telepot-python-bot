from random import randint
class handler:
  def __init__(self):
    tmp = open('./data/data.txt').read().split('\n')
    self.patients = []
    self.data = {}

    for t in tmp:
      t = t.split('#')
      idp = t[0]
      name = t[1]
      surname = t[2]
      address = t[3]

      self.patients.append({'id': idp, 'name': name, 'surname': surname, 'address': address})

  def genData(self):
    for p in self.patients:
      SpO2 = randint(90, 100)
      FC = randint(60, 130)
      Pi = randint(80, 90)
      FR = randint(10, 20)
      self.data[p['id']] = {'SpO2': SpO2, 'FC': FC, 'Pi': Pi, 'FR': FR}

  def getPatientById(self, pid):
    for p in self.patients:
      if (p['id'] == pid):
        return p

    return None


  def testforCritical(self):
    critici = []
    for k in self.data:
      if (self.data[k]['SpO2'] <= 92):
        p = self.getPatientById(k)
        if (not p == None):
          critici.append({'patient': p, 'value': self.data[k]['SpO2']})

    return critici
