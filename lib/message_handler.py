from rivescript import RiveScript
import re

class rive_handler:
  def __init__(self):
    # Inizializza l'interprete di rivescript selezionando la modalita` utf-8
    # in pratica serve a consentire l'utilizzo di caratteri come / nei trigger
    self.bot = RiveScript(utf8=True)
    self.bot.unicode_punctuation = re.compile(r'[.,!?;:"\']')

    # Carica nel bot i vari file .rive dalla cartella specificata, poi riorganizza le risposte
    self.bot.load_directory("./rive")
    self.bot.sort_replies()



  def handle(self, msg, tbot, chat):
    reply = self.bot.reply("localuser", msg)
    if (reply[0] == '~'):
      return reply
    else:
      if (not msg == "nothing"):
        tbot.sendMessage(chat, reply)   
      return None

  def cloneMex(self, msg, tbot, chat):
    tbot.sendMessage(chat, msg)
