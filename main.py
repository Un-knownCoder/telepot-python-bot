import telepot
import time
from lib import data_handler, token
from lib import message_handler

chat = {"id": []}


handler = data_handler.handler()
rive = message_handler.rive_handler()



def on_chat_message(msg):
  content_type, chat_type, chat_id = telepot.glance(msg)
  if (not msg['from']['is_bot']):
    reply = rive.handle(msg['text'], bot, chat_id)
    if (content_type == 'text'):
      try:
        chat_index = chat['id'].index(chat_id)
        if (reply == "~ logout OK ~"):
          del chat['id'][chat_index]
      except:
        if (reply == "~ login OK ~"):
          print("New chat initializated - %s" % (chat_id))
          chat['id'].append(chat_id)

# prendo il token da un file esterno
token = token.getToken()

print('Awaiting (and listening) ...')

bot = telepot.Bot(token)
bot.message_loop(on_chat_message)

while True:
  handler.genData()
  crit = handler.testforCritical()
  
  if (not len(chat['id']) == 0):
    for c in chat['id']:
      for p in crit:
        msg = "CRITICAL PATIENT!!!\n%s %s (that lives in %s) has an SpO2 value of %s" % (p['patient']['surname'], p['patient']['name'], p['patient']['address'], p['value'])
        bot.sendMessage(c, msg)

      bot.sendMessage(c, ".")

  time.sleep(10)
